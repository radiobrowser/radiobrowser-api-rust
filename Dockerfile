FROM ubuntu:noble
ADD . /app
WORKDIR /app
ARG RUST_VERSION
RUN apt-get update && apt-get install -y build-essential curl pkgconf libssl-dev
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs -o rustup-init
RUN sh rustup-init -y --default-toolchain=${RUST_VERSION}
ENV PATH="/root/.cargo/bin:$PATH"
RUN cargo build --release

FROM ubuntu:noble
EXPOSE 8080
RUN apt-get update && apt-get install -y libssl3 && apt-get clean autoclean && apt-get autoremove --yes && rm -rf /var/lib/{apt,dpkg,cache,log}/
COPY --from=0 /app/target/release/radiobrowser-api-rust /usr/bin/
COPY --from=0 /app/static/ /usr/lib/radiobrowser/static/
COPY --from=0 /app/etc/config-example.toml /etc/radiobrowser/config.toml
COPY --from=0 /app/etc/*.csv /etc/radiobrowser/
RUN groupadd -r radiobrowser && \
 useradd -r -g radiobrowser radiobrowser && \
 mkdir -p /var/log/radiobrowser/ && \
 chown -R radiobrowser:radiobrowser /var/log/radiobrowser/ && \
 chmod go+r /etc/radiobrowser/config.toml
ENV STATIC_FILES_DIR=/usr/lib/radiobrowser/static/
USER radiobrowser:radiobrowser
CMD [ "radiobrowser-api-rust", "-f", "/etc/radiobrowser/config.toml", "-vvv"]
