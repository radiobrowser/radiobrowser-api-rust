use uuid::Uuid;

use crate::{config::{get_cache_iso_3166_2, get_station_list_to_delete, get_station_list_to_update}, db::models::DbStationItemEditor, DbConnection};
use std::{collections::HashMap, error::Error};

pub fn do_cleanup<C>(
    delete: bool,
    mut conn_new_style: C,
    click_valid_timeout: u64,
    broken_stations_never_working_timeout: u64,
    broken_stations_timeout: u64,
    checks_timeout: u64,
    clicks_timeout: u64,
    unused_streaming_server_timeout: u64,
    max_station_history: u64,
) -> Result<(), Box<dyn Error>>
where
    C: DbConnection,
{
    let checks_hour = conn_new_style.get_station_count_todo(1)?;
    let checks_day = conn_new_style.get_station_count_todo(24)?;
    let stations_broken = conn_new_style.get_station_count_broken()?;
    let stations_working = conn_new_style.get_station_count_working()?;
    let stations_todo = conn_new_style.get_station_count_todo(24)?;
    let stations_deletable_never_worked =
        conn_new_style.get_deletable_never_working(broken_stations_never_working_timeout)?;
    let stations_deletable_were_working =
        conn_new_style.get_deletable_were_working(broken_stations_timeout)?;
    
    update_stations_by_uuid(&mut conn_new_style)?;

    if delete {
        delete_stations_by_uuid(&conn_new_style)?;
        conn_new_style.delete_never_working(broken_stations_never_working_timeout)?;
        conn_new_style.delete_were_working(broken_stations_timeout)?;
        if max_station_history > 0 {
            conn_new_style.delete_from_history_more_than_entries(max_station_history)?;
        }
        conn_new_style.delete_old_checks(checks_timeout)?;
        conn_new_style.delete_old_clicks(clicks_timeout)?;
        conn_new_style.delete_removed_from_history()?;
        conn_new_style.delete_unused_streaming_servers(unused_streaming_server_timeout)?;
    }

    conn_new_style.update_stations_clickcount()?;
    conn_new_style.remove_unused_ip_infos_from_stationclicks(click_valid_timeout)?;
    conn_new_style.calc_country_field()?;
    calc_country_subdivision_field(&mut conn_new_style)?;

    info!("STATS: {} Checks/Hour, {} Checks/Day, {} Working stations, {} Broken stations, {} to do, deletable {} + {}", checks_hour, checks_day, stations_working, stations_broken, stations_todo, stations_deletable_never_worked, stations_deletable_were_working);
    Ok(())
}

pub fn update_stations_by_uuid<C>(conn: &mut C) -> Result<(), Box<dyn Error>>
where
    C: DbConnection,
{
    debug!("update_stations_by_uuid()");
    let uuid_list_to_update: Vec<DbStationItemEditor> = match get_station_list_to_update() {
        Some(mutex) => match mutex.lock() {
            Ok(map) => map.clone(),
            Err(err) => {
                warn!(
                    "Unable to get station update list from shared memory: {}",
                    err
                );
                vec![]
            }
        },
        None => vec![],
    };

    let len: usize = uuid_list_to_update.len();
    debug!("update_stations_by_uuid() Found stations: {}", len);
    for station in uuid_list_to_update {
        conn.update_station_editor(&station)?;
    }
    debug!("update_stations_by_uuid() update of {} done", len);
    Ok(())
}


pub fn delete_stations_by_uuid<C>(conn: &C) -> Result<(), Box<dyn Error>>
where
    C: DbConnection,
{
    let uuid_list_to_delete: Vec<Uuid> = match get_station_list_to_delete() {
        Some(mutex) => match mutex.lock() {
            Ok(map) => map.clone(),
            Err(err) => {
                warn!(
                    "Unable to get station deletion list from shared memory: {}",
                    err
                );
                vec![]
            }
        },
        None => vec![],
    };
    let uuid_str_list_to_delete: Vec<String> = uuid_list_to_delete
        .into_iter()
        .map(|item| item.to_string())
        .collect();
    conn.delete_stations(&uuid_str_list_to_delete)?;
    Ok(())
}

pub fn calc_country_subdivision_field<C>(conn: &mut C) -> Result<(), Box<dyn Error>>
where
    C: DbConnection,
{
    let map: HashMap<String, String> = match get_cache_iso_3166_2() {
        Some(mutex) => match mutex.lock() {
            Ok(map) => map.clone(),
            Err(err) => {
                warn!(
                    "Unable to get iso-3166-2 mapping list from shared memory: {}",
                    err
                );
                HashMap::new()
            }
        },
        None => HashMap::new(),
    };
    conn.calc_country_subdivision_field(&map)?;
    Ok(())
}
