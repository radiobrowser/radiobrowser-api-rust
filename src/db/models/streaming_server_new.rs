use serde::{Deserialize, Serialize};

#[derive(PartialEq, Eq, Serialize, Deserialize, Debug)]
pub struct DbStreamingServerNew {
    pub url: String,
    pub statusurl: Option<String>,
    pub error: Option<String>,
    pub admin: Option<String>,
    pub location: Option<String>,
    pub software: Option<String>,
}

impl DbStreamingServerNew {
    pub fn new(
        url: String,
        statusurl: Option<String>,
        error: Option<String>,
        admin: Option<String>,
        location: Option<String>,
        software: Option<String>,
    ) -> Self {
        DbStreamingServerNew {
            url,
            statusurl,
            error,
            admin,
            location,
            software,
        }
    }
}
