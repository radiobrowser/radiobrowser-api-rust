use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct DbStationItemEditor {
    pub stationuuid: String,
    pub name: String,
    pub url: String,
    pub homepage: String,
    pub favicon: String,
    pub tags: String,
    pub countrycode: String,
    pub iso_3166_2: Option<String>,
    pub languagecodes: String,
    pub geo_lat: Option<f64>,
    pub geo_long: Option<f64>,
}
