use reqwest::Url;
use serde::Deserialize;
use uuid::Uuid;
use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::Read;

use crate::db::models::DbStationItemEditor;

#[derive(Debug, Clone, Deserialize)]
struct DataMappingItem {
    from: String,
    to: String,
}

#[derive(Debug, Clone, Deserialize)]
struct DataListItem {
    uuid: Uuid,
}

pub fn read_list_station_json_file(file_path: &str) -> Result<Vec<DbStationItemEditor>, Box<dyn Error>> {
    debug!("read_list_station_json_file()");
    match Url::parse(file_path) {
        Ok(url) => {
            debug!("Remote url: {}", url);
            read_list_station_json_file_reader(reqwest::blocking::get(url)?)
        }
        Err(_) => {
            debug!("Local path: {}", file_path);
            read_list_station_json_file_reader(File::open(file_path)?)
        }
    }
}

pub fn read_list_csv_file(file_path: &str) -> Result<Vec<Uuid>, Box<dyn Error>> {
    debug!("read_list_csv_file()");
    match Url::parse(file_path) {
        Ok(url) => {
            debug!("Remote url: {}", url);
            read_list_csv_file_reader(reqwest::blocking::get(url)?)
        }
        Err(_) => {
            debug!("Local path: {}", file_path);
            read_list_csv_file_reader(File::open(file_path)?)
        }
    }
}

pub fn read_map_csv_file(file_path: &str) -> Result<HashMap<String, String>, Box<dyn Error>> {
    debug!("read_map_csv_file()");
    match Url::parse(file_path) {
        Ok(url) => {
            debug!("Remote url: {}", url);
            read_map_csv_file_reader(reqwest::blocking::get(url)?)
        }
        Err(_) => {
            debug!("Local path: {}", file_path);
            read_map_csv_file_reader(File::open(file_path)?)
        }
    }
}

fn read_list_station_json_file_reader<R>(reader: R) -> Result<Vec<DbStationItemEditor>, Box<dyn Error>>
where
    R: Read,
{
    debug!("read_list_station_json_file_reader() loading..");
    let items: Vec<DbStationItemEditor> = serde_json::from_reader(reader)?;
    debug!("read_list_station_json_file_reader() loaded {} items", items.len());
    Ok(items)
}

fn read_list_csv_file_reader<R>(reader: R) -> Result<Vec<Uuid>, Box<dyn Error>>
where
    R: Read,
{
    debug!("read_list_csv_file_reader() loading.. ");
    let mut rdr = csv::ReaderBuilder::new()
        .has_headers(true)
        .delimiter(b';')
        .comment(Some(b'#'))
        .from_reader(reader);
    let mut r: Vec<Uuid> = vec![];
    for result in rdr.deserialize() {
        let record: DataListItem = result?;
        warn!("loaded record: {:?}", record);
        if !r.contains(&record.uuid) {
            r.push(record.uuid);
        } else {
            error!("Duplicate key in file: {}", record.uuid);
        }
    }
    debug!("read_list_csv_file_reader() loaded {} items", r.len());
    Ok(r)
}

fn read_map_csv_file_reader<R>(reader: R) -> Result<HashMap<String, String>, Box<dyn Error>>
where
    R: Read,
{
    debug!("read_map_csv_file_reader() loading..");
    let mut rdr = csv::ReaderBuilder::new()
        .has_headers(true)
        .delimiter(b';')
        .comment(Some(b'#'))
        .from_reader(reader);
    let mut r: HashMap<String, String> = HashMap::new();
    for result in rdr.deserialize() {
        let record: DataMappingItem = result?;
        trace!("loaded record: {:?}", record);
        if !r.contains_key(&record.from) {
            r.insert(record.from, record.to);
        } else {
            error!("Duplicate key in file: {}", record.from);
        }
    }
    debug!("read_map_csv_file_reader() loaded {} items", r.len());
    Ok(r)
}
